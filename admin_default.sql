/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : admin_default

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 03/04/2024 16:15:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for portal_user
-- ----------------------------

create database biology;
use biology;

DROP TABLE IF EXISTS `portal_user`;
CREATE TABLE `portal_user` (
  `id` bigint NOT NULL,
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `phone_num` varchar(100) DEFAULT NULL COMMENT '手机号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of portal_user
-- ----------------------------
BEGIN;
INSERT INTO `portal_user` (`id`, `username`, `phone_num`, `password`, `CREATE_TIME`) VALUES (1772103129710469122, 'fengpan', '121', '1', '2024-03-25 11:28:05');
INSERT INTO `portal_user` (`id`, `username`, `phone_num`, `password`, `CREATE_TIME`) VALUES (1772605551570698242, 'feng1', '13637070005', '1', '2024-03-26 20:44:32');
COMMIT;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` bigint NOT NULL,
  `user_id` bigint DEFAULT NULL COMMENT '发帖用户ID',
  `title` varchar(200) DEFAULT NULL COMMENT '帖子标题',
  `preview_content` varchar(500) DEFAULT NULL,
  `content` text COMMENT '帖子内容',
  `create_time` datetime DEFAULT NULL COMMENT '发帖时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of post
-- ----------------------------
BEGIN;
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (1, 1772103129710469122, 'How to cook a perfect steak', 'Are you tired of eating mediocre steak? Learn how to cook the perfect steak with these simple steps!', 'To cook a perfect steak, start by choosing a high-quality cut of meat such as ribeye or filet mignon. Let the steak come to room temperature, then season generously with salt and pepper. Heat a skillet or grill to high heat, then sear the steak for 2-3 minutes on each side for a nice crust. For medium-rare, cook until the internal temperature reaches 135°F. Let the steak rest for a few minutes before slicing and serving. Enjoy!', '2024-03-30 18:00:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (2, 1772103129710469122, 'Exploring the wonders of nature', 'Discover the beauty of nature and all its wonders with our latest expedition!', 'Join us on an adventure as we explore breathtaking landscapes, encounter fascinating wildlife, and immerse ourselves in the wonders of the natural world. From lush rainforests to towering mountains, there is so much to see and experience. Pack your bags and get ready for an unforgettable journey!', '2024-03-30 18:30:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (3, 1772103129710469122, 'The art of photography', 'Capture moments that last a lifetime with the art of photography.', 'Photography is more than just taking pictures - its about capturing emotions, telling stories, and preserving memories. Whether you are a beginner or an experienced photographer, theres always something new to learn and explore. Join us as we delve into the world of photography and unlock your creative potential!', '2024-03-30 19:00:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (4, 1772103129710469122, 'Healthy eating habits for a better life', 'Transform your health and well-being with these simple yet effective eating habits.', 'Eating healthy doesnt have to be complicated or boring. With the right mindset and approach, you can nourish your body with delicious and nutritious foods that will leave you feeling energized and satisfied. From meal prep tips to easy recipes, weve got everything you need to make healthy eating a part of your daily routine. Say goodbye to crash diets and hello to a happier, healthier you!', '2024-03-30 19:30:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (5, 1772103129710469122, 'The power of mindfulness', 'Discover the transformative power of mindfulness and unlock your full potential.', 'Mindfulness is the practice of being present in the moment and cultivating a state of awareness and acceptance. By incorporating mindfulness into your daily life, you can reduce stress, improve focus and concentration, and enhance overall well-being. Join us as we explore different mindfulness techniques and learn how to live with greater intention and clarity.', '2024-03-30 20:00:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (6, 1772103129710469122, 'Tips for mastering the art of public speaking', 'Overcome your fear of public speaking and become a confident and compelling communicator.', 'Public speaking is a valuable skill that can open doors to new opportunities and enhance your personal and professional life. Whether you are delivering a presentation at work or speaking at a social event, it  is important to feel confident and prepared. In this workshop, we   will share practical tips and strategies for conquering your fear of public speaking and delivering memorable and impactful speeches.', '2024-03-30 20:30:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (7, 1772103129710469122, 'Unlocking the secrets of the universe', 'Explore the mysteries of the cosmos and expand your understanding of the universe.', 'From the origins of the universe to the search for extraterrestrial life, there are countless questions waiting to be answered. Join us as we embark on a journey through space and time, guided by the latest discoveries and breakthroughs in astrophysics and cosmology. Whether you are a seasoned astronomer or a curious novice, there  is something for everyone in this cosmic adventure!', '2024-03-30 21:00:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (8, 1772103129710469122, 'The art of self-care', 'Prioritize your well-being and practice self-care with these simple yet effective strategies.', 'Self-care is essential for maintaining physical, mental, and emotional health. It  is about taking time to nurture yourself and prioritize your needs in a world that often demands so much of us. Join us as we explore different self-care practices and learn how to cultivate a lifestyle that promotes balance, resilience, and inner peace.', '2024-03-30 21:30:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (9, 1772103129710469122, 'Exploring the world of literature', 'Dive into the rich and diverse world of literature with our latest book club selection.', 'From classic novels to contemporary bestsellers, literature has the power to transport us to different worlds, spark our imagination, and challenge our perspectives. Join our book club as we discuss our latest reading selection and share our thoughts, insights, and favorite passages. Whether you are a voracious reader or just looking for your next great read, you are sure to find something to love in the world of literature.', '2024-03-30 22:00:00');
INSERT INTO `post` (`id`, `user_id`, `title`, `preview_content`, `content`, `create_time`) VALUES (10, 1772103129710469122, 'Mastering the art of negotiation', 'Learn how to negotiate effectively and achieve win-win outcomes in any situation.', 'Negotiation is a fundamental skill that can help you navigate life  is challenges and seize opportunities for success. Whether you are bargaining for a higher salary, closing a business deal, or resolving conflicts in your personal relationships, effective negotiation skills are essential for achieving your goals. Join us as we explore the principles of negotiation and learn practical strategies for achieving mutually beneficial agreements.', '2024-03-30 22:30:00');
COMMIT;

-- ----------------------------
-- Table structure for post_comment
-- ----------------------------
DROP TABLE IF EXISTS `post_comment`;
CREATE TABLE `post_comment` (
  `id` bigint NOT NULL,
  `post_id` bigint DEFAULT NULL COMMENT '帖子ID',
  `user_id` bigint DEFAULT NULL COMMENT '评论用户ID',
  `comment_content` text COMMENT '评论内容',
  `create_time` datetime DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of post_comment
-- ----------------------------
BEGIN;
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (1, 1, 201, 'Thanks for the cooking tips! Can\'t wait to try it out!', '2024-03-30 18:05:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (2, 1, 202, 'I tried this recipe last night and it was amazing! Definitely a game-changer.', '2024-03-30 18:15:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (3, 1, 203, 'I never knew cooking steak could be so easy! Thanks for sharing.', '2024-03-30 18:25:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (4, 1, 204, 'This is now my go-to recipe for steak. Thanks for the detailed instructions!', '2024-03-30 18:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (5, 2, 201, 'Nature truly is incredible. Can\'t wait to see more!', '2024-03-30 18:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (6, 2, 202, 'The photos from your expedition are breathtaking! Keep them coming.', '2024-03-30 18:45:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (7, 2, 203, 'I wish I could be there with you! Looks like an amazing adventure.', '2024-03-30 18:55:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (8, 3, 201, 'Photography has always been a passion of mine. Can\'t wait to learn more!', '2024-03-30 19:05:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (9, 3, 202, 'Do you have any tips for beginner photographers?', '2024-03-30 19:15:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (10, 3, 203, 'I\'ve always wanted to improve my photography skills. Excited for this!', '2024-03-30 19:25:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (11, 4, 201, 'Healthy eating is so important. Thanks for the reminder!', '2024-03-30 19:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (12, 4, 202, 'I struggle with meal prep. Any tips for making it easier?', '2024-03-30 19:45:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (13, 4, 203, 'I\'ve been trying to eat healthier lately. Can\'t wait to try out your recipes!', '2024-03-30 19:55:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (14, 5, 201, 'Mindfulness has changed my life in so many ways. Excited to learn more!', '2024-03-30 20:05:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (15, 5, 202, 'I\'ve heard so many great things about mindfulness. Can\'t wait to get started!', '2024-03-30 20:15:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (16, 5, 203, 'Do you have any recommendations for mindfulness apps?', '2024-03-30 20:25:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (17, 6, 201, 'Public speaking terrifies me. Hoping to overcome my fear with your tips!', '2024-03-30 20:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (18, 6, 202, 'I\'ve always struggled with public speaking. Excited to learn from the experts!', '2024-03-30 20:45:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (19, 6, 203, 'Any advice for calming nerves before a big presentation?', '2024-03-30 20:55:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (20, 7, 201, 'The universe is so vast and mysterious. Can\'t wait to learn more!', '2024-03-30 21:05:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (21, 7, 202, 'I\'ve always been fascinated by space. Excited for this cosmic adventure!', '2024-03-30 21:15:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (22, 7, 203, 'Do you believe in extraterrestrial life?', '2024-03-30 21:25:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (23, 8, 201, 'Self-care is so important, especially in today\'s hectic world.', '2024-03-30 21:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (24, 8, 202, 'I often forget to prioritize my own well-being. Thanks for the reminder!', '2024-03-30 21:45:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (25, 8, 203, 'Any recommendations for practicing self-care on a budget?', '2024-03-30 21:55:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (26, 9, 201, 'I\'ve been looking for a new book to read. Can\'t wait to join the book club!', '2024-03-30 22:05:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (27, 9, 202, 'Do you have any suggestions for our next book club pick?', '2024-03-30 22:15:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (28, 9, 203, 'I love discussing books with others. Excited to join the club!', '2024-03-30 22:25:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (29, 10, 201, 'Negotiation is such an important skill, both in business and in life.', '2024-03-30 22:35:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (30, 10, 202, 'I\'ve always struggled with negotiation. Hoping to improve with your guidance!', '2024-03-30 22:45:00');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (1774100267025952770, 10, 1772605551570698242, 'this  is my test', '2024-03-30 23:43:59');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (1774105913293139969, 5, 1772605551570698242, '1212', '2024-03-31 00:06:26');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (1774106795485294593, 10, 1770282036322385922, 'dddddddddd', '2024-03-31 00:09:56');
INSERT INTO `post_comment` (`id`, `post_id`, `user_id`, `comment_content`, `create_time`) VALUES (1774106869615423489, 1, 1770282036322385922, 'wwwwwwwwwwwwwwwwwwwww', '2024-03-31 00:10:14');
COMMIT;

-- ----------------------------
-- Table structure for SYS_MENU
-- ----------------------------
DROP TABLE IF EXISTS `SYS_MENU`;
CREATE TABLE `SYS_MENU` (
  `MENU_ID` bigint NOT NULL COMMENT '菜单ID',
  `MENU_NAME` varchar(50) NOT NULL COMMENT '菜单名称',
  `MENU_TYPE` varchar(255) DEFAULT NULL COMMENT '菜单类型 1.目录 2.菜单',
  `PARENT_ID` bigint DEFAULT '0' COMMENT '父菜单ID',
  `ORDER_NUM` int DEFAULT '0' COMMENT '显示顺序',
  `VISIBLE` varchar(255) DEFAULT NULL COMMENT '是否可见 1.可见 2.隐藏',
  `PATH` varchar(200) DEFAULT '' COMMENT '路由地址',
  `ICON` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `DEFAULT_MENU` varchar(10) DEFAULT NULL COMMENT '默认菜单 1.是 2.否',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单权限表';

-- ----------------------------
-- Records of SYS_MENU
-- ----------------------------
BEGIN;
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1, '系统管理', '1', 0, 100, '1', '', '#', '1', NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (2, '用户管理', '2', 1, 1, '1', 'sys/user', '#', '1', NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (3, '菜单管理', '2', 1, 2, '1', 'sys/menu', '232', '1', NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (4, '角色管理', '2', 1, 2, '1', 'sys/role', '11', '1', NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1775417731298914306, '公告管理', '2', 0, 5, '1', 'notice', '1', NULL, NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1775418583749259266, '个人成果管理', '2', 0, 3, '1', 'personalAchievement', '1', NULL, NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1775418771591163906, '个人学术履历主页生成', '2', 0, 4, '1', 'ResumeHomepageGen', '1', NULL, NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1775419149959327746, '课题组成果管理', '2', 0, 6, '1', 'groupAchievement', '1', NULL, NULL);
INSERT INTO `SYS_MENU` (`MENU_ID`, `MENU_NAME`, `MENU_TYPE`, `PARENT_ID`, `ORDER_NUM`, `VISIBLE`, `PATH`, `ICON`, `DEFAULT_MENU`, `CREATE_TIME`) VALUES (1775419328536014850, '成员管理', '2', 0, 8, '1', 'memberManage', '1', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for SYS_ROLE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE` (
  `ROLE_ID` bigint NOT NULL COMMENT '角色ID',
  `ROLE_NAME` varchar(30) NOT NULL COMMENT '角色名称',
  `ROLE_KEY` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `DEFAULT_ROLE` varchar(10) DEFAULT NULL COMMENT '默认角色， 1.是 2.否',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色信息表';

-- ----------------------------
-- Records of SYS_ROLE
-- ----------------------------
BEGIN;
INSERT INTO `SYS_ROLE` (`ROLE_ID`, `ROLE_NAME`, `ROLE_KEY`, `DEFAULT_ROLE`, `CREATE_TIME`) VALUES (1, '超级管理员', 'super_admin', '1', NULL);
INSERT INTO `SYS_ROLE` (`ROLE_ID`, `ROLE_NAME`, `ROLE_KEY`, `DEFAULT_ROLE`, `CREATE_TIME`) VALUES (1770268605515157506, '业务管理员', 'admin', '1', NULL);
COMMIT;

-- ----------------------------
-- Table structure for SYS_ROLE_MENU
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE_MENU`;
CREATE TABLE `SYS_ROLE_MENU` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `ROLE_ID` bigint NOT NULL COMMENT '角色ID',
  `MENU_ID` bigint NOT NULL COMMENT '菜单ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of SYS_ROLE_MENU
-- ----------------------------
BEGIN;
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (29, 1, 2, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (30, 1, 3, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (31, 1, 4, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (32, 1, 1775417731298914306, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (33, 1, 1775418771591163906, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (34, 1, 1775419328536014850, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (35, 1, 1775418583749259266, '2024-04-03 15:05:41');
INSERT INTO `SYS_ROLE_MENU` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`) VALUES (36, 1, 1775419149959327746, '2024-04-03 15:05:41');
COMMIT;

-- ----------------------------
-- Table structure for SYS_ROLE_RELATION
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE_RELATION`;
CREATE TABLE `SYS_ROLE_RELATION` (
  `ID` bigint NOT NULL,
  `USER_ID` bigint NOT NULL,
  `ROLE_ID` bigint NOT NULL,
  `CREATED_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of SYS_ROLE_RELATION
-- ----------------------------
BEGIN;
INSERT INTO `SYS_ROLE_RELATION` (`ID`, `USER_ID`, `ROLE_ID`, `CREATED_TIME`) VALUES (1772003850010533890, 1770282036322385922, 1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for SYS_USER
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER` (
  `USER_ID` bigint NOT NULL COMMENT '用户ID',
  `ACCOUNT` varchar(255) NOT NULL COMMENT '用户账号',
  `PASSWORD` varchar(255) NOT NULL COMMENT '密码',
  `AVATAR` text COMMENT '头像',
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `GENDER` varchar(10) DEFAULT NULL COMMENT '1 男 0 女',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '状态 1.正常 2.封禁',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';

-- ----------------------------
-- Records of SYS_USER
-- ----------------------------
BEGIN;
INSERT INTO `SYS_USER` (`USER_ID`, `ACCOUNT`, `PASSWORD`, `AVATAR`, `USERNAME`, `EMAIL`, `PHONE`, `GENDER`, `STATUS`, `CREATE_TIME`) VALUES (1770282036322385922, 'superadmin', '1', 'data:image/jpg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCABkAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDvKKKK/IT6sKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigC1YWT39wYUZVIUtlqvyeHLiOJ3M0RCgk4zWXbW0t3MIoV3ORkDOK6HW7KeW0tmRQRAjGTkccD/A17eAwtKrhKlWVJycdmm9fw6bs4q9WUasYqSSf9fiQHw5EsixnUFEjchSgyfwzSHw7H+8C36s6DLKEGR9ea15mthqturwu05U7JB0Uc9efrTY2tjdXwihdZQP3jno3HbmvfeVYDmceRbtfFK/w3+/8ACxwrFV7Xu9uy7nGVuyaDbQhfO1FIywyAwA/rWNBC9xMsUYy7HAGcV1ssck4XztLWQqMAtIteFk+Dp14VJVIc21tJW8/h+R3YurKDiou3fb9TKTRLKRwiapGzHoBgk/rWXfWws7yS3D79mPmxjPGa6eO38qQPHpMauvQiReKwdaimW/aaaPy/N5C7gegArbNMDSpYbnhTtK/RTta397zIw1aUqlnK6t5b/IzqKKK+cPQCiiigAooooAfDJLFIGhZlfsVPNdPq0V7NaQeQzBRGTL82M8Dr69656xvpLCczRqrEqVw3SrQ1S4vbhI7q5aOBjh/L+XAr2cBiaFPDzozk7zsrLRLXe/n102OOvTnKopJKy/rY6G3Oq+ev2gW3lfxbM56U25GqtJIsItvKPC7s5xVO3bSradZl1GZivZ2yDx9KSc6VcTPK2ozKWOSFfAH6V9I8QnR5efW//PxXtbvb8Dz+T378un+FmPpH/IWt/wDe/pV+8luJPERt0nmVGdRtVyABgZqvYW9u19K6XwgELZjZgPmGTzzWibvTbKZ7kzNdXTD7wA9MduBXiYKlbCKE5qMee7d1qkrNJJ3v8jsrS/e3iru1tupS1maS1v8Ayre4nVQgyPNY8/ifpUviX/W23+4aii1a0mLi+sg5dixkXk/5x70/VRZ3FsLmK9Z2X5Vibk/4iqrThVw9eVKaalZpXs0k77O34CgnGcFJNW697/11MSiiivmz0QooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9k=', '超级管理员', 'fp123@gmail.com', '18505666341', '1', NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `files_info` (
                              `id` bigint NOT NULL,
                              `owner_id` bigint DEFAULT NULL,
                              `url` varchar(300) DEFAULT NULL,
                              `type` varchar(10) DEFAULT NULL,
                              `base64_content` text,
                              `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

