package com.fp.admintemplate;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.fp.**.mapper")
@Slf4j
public class AdminTemplateApplication {

    public static void main(String[] args) {

        SpringApplication.run(AdminTemplateApplication.class, args);
    }

}
