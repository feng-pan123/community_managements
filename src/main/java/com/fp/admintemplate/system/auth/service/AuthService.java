package com.fp.admintemplate.system.auth.service;


import com.fp.admintemplate.system.auth.entity.AuthLoginRequest;
import com.fp.admintemplate.system.auth.entity.SysLoginUser;
import com.fp.admintemplate.system.entity.domain.SysUser;

public interface AuthService  {


    String doLogin(AuthLoginRequest authLoginRequest);

    SysLoginUser getLoginUser();

    void createAccount(AuthLoginRequest request);
    SysUser createAccountWithRole(AuthLoginRequest request, String roleKey);


    void resetPassword(AuthLoginRequest request);

    void doLogout();

    void deleteAccount(AuthLoginRequest request);
}
