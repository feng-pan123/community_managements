package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.PaymentManagementDto;
import com.fp.admintemplate.business.entity.PaymentManagementRequest;
import com.fp.admintemplate.business.entity.domain.PaymentManagement;
import com.fp.admintemplate.business.mapper.PaymentManagementMapper;
import com.fp.admintemplate.business.service.PaymentManagementService;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class PaymentManagementServiceImpl extends ServiceImpl<PaymentManagementMapper, PaymentManagement>
        implements PaymentManagementService {


    @Override
    public Page<PaymentManagementDto> getPaymentManagementPage(PaymentManagementRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectPaymentManagementPage(request, objectPage);
    }

    @Override
    public void addPaymentManagement(PaymentManagementRequest request) {
        PaymentManagement paymentManagement = BeanUtil.copyProperties(request, PaymentManagement.class);
        paymentManagement.setCreateTime(new Date());
        this.baseMapper.insert(paymentManagement);
    }

    @Override
    public void deletePaymentManagement(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void updatePaymentManagement(PaymentManagementRequest request) {
        PaymentManagement paymentManagement = BeanUtil.copyProperties(request, PaymentManagement.class);
        this.baseMapper.updateById(paymentManagement);
    }

    @Override
    public PaymentManagementDto getPaymentManagementOne(Long id) {
        LambdaQueryWrapper<PaymentManagement> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PaymentManagement::getId, id);
        PaymentManagement one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, PaymentManagementDto.class);
    }

}




