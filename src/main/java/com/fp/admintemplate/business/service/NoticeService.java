package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.NoticeDto;
import com.fp.admintemplate.business.entity.NoticeRequest;
import com.fp.admintemplate.business.entity.domain.Notice;

import java.util.List;


public interface NoticeService extends IService<Notice> {

    Page<NoticeDto> getNoticePage(NoticeRequest request);
    List<NoticeDto> getNoticeList(NoticeRequest request);

    void addNotice(NoticeRequest request);

    void deleteNotice(Long id);

    void updateNotice(NoticeRequest request);

    NoticeDto getNoticeOne(Long id);


}
