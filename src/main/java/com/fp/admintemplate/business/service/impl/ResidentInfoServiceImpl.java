package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.ResidentInfoDto;
import com.fp.admintemplate.business.entity.ResidentInfoRequest;
import com.fp.admintemplate.business.entity.domain.ResidentInfo;
import com.fp.admintemplate.business.mapper.ResidentInfoMapper;
import com.fp.admintemplate.business.service.ResidentInfoService;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class ResidentInfoServiceImpl extends ServiceImpl<ResidentInfoMapper, ResidentInfo>
        implements ResidentInfoService {


    @Override
    public Page<ResidentInfoDto> getResidentInfoPage(ResidentInfoRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectResidentInfoPage(request, objectPage);
    }

    @Override
    public void addResidentInfo(ResidentInfoRequest request) {
        ResidentInfo residentInfo = BeanUtil.copyProperties(request, ResidentInfo.class);
        residentInfo.setCreateTime(new Date());
        this.baseMapper.insert(residentInfo);
    }

    @Override
    public void deleteResidentInfo(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void updateResidentInfo(ResidentInfoRequest request) {
        ResidentInfo residentInfo = BeanUtil.copyProperties(request, ResidentInfo.class);
        this.baseMapper.updateById(residentInfo);
    }

    @Override
    public ResidentInfoDto getResidentInfoOne(Long id) {
        LambdaQueryWrapper<ResidentInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ResidentInfo::getId, id);
        ResidentInfo one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, ResidentInfoDto.class);
    }

}




