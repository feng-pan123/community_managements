package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.TeamMembersDto;
import com.fp.admintemplate.business.entity.TeamMembersRequest;
import com.fp.admintemplate.business.entity.domain.TeamMembers;
import com.fp.admintemplate.business.mapper.TeamMembersMapper;
import com.fp.admintemplate.business.service.TeamMembersService;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class TeamMembersServiceImpl extends ServiceImpl<TeamMembersMapper, TeamMembers>
        implements TeamMembersService {


    @Override
    public Page<TeamMembersDto> getTeamMembersPage(TeamMembersRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectTeamMembersPage(request, objectPage);
    }

    @Override
    public void addTeamMembers(TeamMembersRequest request) {
        TeamMembers teamMembers = BeanUtil.copyProperties(request, TeamMembers.class);
        teamMembers.setCreateTime(new Date());
        this.baseMapper.insert(teamMembers);
    }

    @Override
    public void deleteTeamMembers(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void updateTeamMembers(TeamMembersRequest request) {
        TeamMembers teamMembers = BeanUtil.copyProperties(request, TeamMembers.class);
        this.baseMapper.updateById(teamMembers);
    }

    @Override
    public TeamMembersDto getTeamMembersOne(Long id) {
        LambdaQueryWrapper<TeamMembers> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TeamMembers::getId, id);
        TeamMembers one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, TeamMembersDto.class);
    }

}




