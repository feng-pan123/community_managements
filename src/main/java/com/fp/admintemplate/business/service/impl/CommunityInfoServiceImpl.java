package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.CommunityInfoDto;
import com.fp.admintemplate.business.entity.CommunityInfoRequest;
import com.fp.admintemplate.business.entity.domain.CommunityInfo;
import com.fp.admintemplate.business.mapper.CommunityInfoMapper;
import com.fp.admintemplate.business.service.CommunityInfoService;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class CommunityInfoServiceImpl extends ServiceImpl<CommunityInfoMapper, CommunityInfo>
        implements CommunityInfoService {


    @Override
    public Page<CommunityInfoDto> getCommunityInfoPage(CommunityInfoRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectCommunityInfoPage(request, objectPage);
    }

    @Override
    public void addCommunityInfo(CommunityInfoRequest request) {
        CommunityInfo communityInfo = BeanUtil.copyProperties(request, CommunityInfo.class);
        communityInfo.setCreateTime(new Date());
        this.baseMapper.insert(communityInfo);
    }

    @Override
    public void deleteCommunityInfo(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void updateCommunityInfo(CommunityInfoRequest request) {
        CommunityInfo communityInfo = BeanUtil.copyProperties(request, CommunityInfo.class);
        this.baseMapper.updateById(communityInfo);
    }

    @Override
    public CommunityInfoDto getCommunityInfoOne(Long id) {
        LambdaQueryWrapper<CommunityInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommunityInfo::getId, id);
        CommunityInfo one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, CommunityInfoDto.class);
    }

}




