package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.CommunityInfoDto;
import com.fp.admintemplate.business.entity.CommunityInfoRequest;
import com.fp.admintemplate.business.entity.domain.CommunityInfo;

import java.util.List;


public interface CommunityInfoService extends IService<CommunityInfo> {

    Page<CommunityInfoDto> getCommunityInfoPage(CommunityInfoRequest request);

    void addCommunityInfo(CommunityInfoRequest request);

    void deleteCommunityInfo(Long id);

    void updateCommunityInfo(CommunityInfoRequest request);

    CommunityInfoDto getCommunityInfoOne(Long id);


}
