package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.PaymentManagementDto;
import com.fp.admintemplate.business.entity.PaymentManagementRequest;
import com.fp.admintemplate.business.entity.domain.PaymentManagement;

import java.util.List;


public interface PaymentManagementService extends IService<PaymentManagement> {

    Page<PaymentManagementDto> getPaymentManagementPage(PaymentManagementRequest request);

    void addPaymentManagement(PaymentManagementRequest request);

    void deletePaymentManagement(Long id);

    void updatePaymentManagement(PaymentManagementRequest request);

    PaymentManagementDto getPaymentManagementOne(Long id);


}
