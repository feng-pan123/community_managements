package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.NoticeDto;
import com.fp.admintemplate.business.entity.NoticeRequest;
import com.fp.admintemplate.business.entity.domain.Notice;
import com.fp.admintemplate.business.mapper.NoticeMapper;
import com.fp.admintemplate.business.service.NoticeService;
import com.fp.admintemplate.common.annotion.ActionType;
import com.fp.admintemplate.common.annotion.FileInfoAction;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;


@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice>
        implements NoticeService {


    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.SET)
    public List<NoticeDto> getNoticeList(NoticeRequest request) {
        List<Notice> list = this.list().stream().sorted(Comparator.comparing(Notice::getCreateTime).reversed()).toList();
        return BeanUtil.copyToList(list,NoticeDto.class);
    }

    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.SET)
    public Page<NoticeDto> getNoticePage(NoticeRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectNoticePage(request, objectPage);
    }

    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.SAVE)
    public void addNotice(NoticeRequest request) {
        Notice notice = BeanUtil.copyProperties(request, Notice.class);
        notice.setCreateTime(new Date());
        this.baseMapper.insert(notice);
        request.setId(notice.getId());
    }

    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.DELETE)
    public void deleteNotice(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.UPDATE)
    public void updateNotice(NoticeRequest request) {
        Notice notice = BeanUtil.copyProperties(request, Notice.class);
        this.baseMapper.updateById(notice);
    }

    @Override
    @FileInfoAction(typeName = "notice",action = ActionType.SET)
    public NoticeDto getNoticeOne(Long id) {
        LambdaQueryWrapper<Notice> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Notice::getId, id);
        Notice one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, NoticeDto.class);
    }

}




