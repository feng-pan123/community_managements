package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.RepairManagementDto;
import com.fp.admintemplate.business.entity.RepairManagementRequest;
import com.fp.admintemplate.business.entity.domain.RepairManagement;

import java.util.List;


public interface RepairManagementService extends IService<RepairManagement> {

    Page<RepairManagementDto> getRepairManagementPage(RepairManagementRequest request);

    void addRepairManagement(RepairManagementRequest request);

    void deleteRepairManagement(Long id);

    void updateRepairManagement(RepairManagementRequest request);

    RepairManagementDto getRepairManagementOne(Long id);


}
