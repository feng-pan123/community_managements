package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.ResidentInfoDto;
import com.fp.admintemplate.business.entity.ResidentInfoRequest;
import com.fp.admintemplate.business.entity.domain.ResidentInfo;

import java.util.List;


public interface ResidentInfoService extends IService<ResidentInfo> {

    Page<ResidentInfoDto> getResidentInfoPage(ResidentInfoRequest request);

    void addResidentInfo(ResidentInfoRequest request);

    void deleteResidentInfo(Long id);

    void updateResidentInfo(ResidentInfoRequest request);

    ResidentInfoDto getResidentInfoOne(Long id);


}
