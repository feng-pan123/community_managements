package com.fp.admintemplate.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.business.entity.RepairManagementDto;
import com.fp.admintemplate.business.entity.RepairManagementRequest;
import com.fp.admintemplate.business.entity.domain.RepairManagement;
import com.fp.admintemplate.business.mapper.RepairManagementMapper;
import com.fp.admintemplate.business.service.RepairManagementService;
import com.fp.admintemplate.system.entity.CommonPageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class RepairManagementServiceImpl extends ServiceImpl<RepairManagementMapper, RepairManagement>
        implements RepairManagementService {


    @Override
    public Page<RepairManagementDto> getRepairManagementPage(RepairManagementRequest request) {
        Page<Object> objectPage = CommonPageRequest.defaultPage().setSize(10).setCurrent(request.getCurrent());
        return this.baseMapper.selectRepairManagementPage(request, objectPage);
    }

    @Override
    public void addRepairManagement(RepairManagementRequest request) {
        RepairManagement repairManagement = BeanUtil.copyProperties(request, RepairManagement.class);
        repairManagement.setCreateTime(new Date());
        this.baseMapper.insert(repairManagement);
    }

    @Override
    public void deleteRepairManagement(Long id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void updateRepairManagement(RepairManagementRequest request) {
        RepairManagement repairManagement = BeanUtil.copyProperties(request, RepairManagement.class);
        this.baseMapper.updateById(repairManagement);
    }

    @Override
    public RepairManagementDto getRepairManagementOne(Long id) {
        LambdaQueryWrapper<RepairManagement> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RepairManagement::getId, id);
        RepairManagement one = this.baseMapper.selectOne(queryWrapper);
        return BeanUtil.copyProperties(one, RepairManagementDto.class);
    }

}




