package com.fp.admintemplate.business.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.business.entity.TeamMembersDto;
import com.fp.admintemplate.business.entity.TeamMembersRequest;
import com.fp.admintemplate.business.entity.domain.TeamMembers;

import java.util.List;


public interface TeamMembersService extends IService<TeamMembers> {

    Page<TeamMembersDto> getTeamMembersPage(TeamMembersRequest request);

    void addTeamMembers(TeamMembersRequest request);

    void deleteTeamMembers(Long id);

    void updateTeamMembers(TeamMembersRequest request);

    TeamMembersDto getTeamMembersOne(Long id);


}
