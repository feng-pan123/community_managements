package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.CommunityInfoDto;
import com.fp.admintemplate.business.entity.CommunityInfoRequest;
import com.fp.admintemplate.business.service.CommunityInfoService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/communityInfo")
public class CommunityInfoController {

    private final CommunityInfoService communityInfoService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody CommunityInfoRequest request) {
        communityInfoService.addCommunityInfo(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        communityInfoService.deleteCommunityInfo(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updateCommunityInfo(@RequestBody CommunityInfoRequest request) {
        communityInfoService.updateCommunityInfo(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<CommunityInfoDto>> page(@RequestBody CommunityInfoRequest request) {
        Page<CommunityInfoDto> communityInfoPage = communityInfoService.getCommunityInfoPage(request);
        return CommonResult.data(communityInfoPage);
    }


    @GetMapping("/one/{id}")
    public CommonResult<CommunityInfoDto> saveMenu(@PathVariable Long id) {
        CommunityInfoDto communityInfoOne = communityInfoService.getCommunityInfoOne(id);
        return CommonResult.data(communityInfoOne);
    }
}
