package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.TeamMembersDto;
import com.fp.admintemplate.business.entity.TeamMembersRequest;
import com.fp.admintemplate.business.service.TeamMembersService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/teamMembers")
public class TeamMembersController {

    private final TeamMembersService teamMembersService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody TeamMembersRequest request) {
        teamMembersService.addTeamMembers(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        teamMembersService.deleteTeamMembers(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updateTeamMembers(@RequestBody TeamMembersRequest request) {
        teamMembersService.updateTeamMembers(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<TeamMembersDto>> page(@RequestBody TeamMembersRequest request) {
        Page<TeamMembersDto> teamMembersPage = teamMembersService.getTeamMembersPage(request);
        return CommonResult.data(teamMembersPage);
    }


    @GetMapping("/one/{id}")
    public CommonResult<TeamMembersDto> saveMenu(@PathVariable Long id) {
        TeamMembersDto teamMembersOne = teamMembersService.getTeamMembersOne(id);
        return CommonResult.data(teamMembersOne);
    }
}
