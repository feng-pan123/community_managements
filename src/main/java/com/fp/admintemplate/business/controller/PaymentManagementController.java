package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.PaymentManagementDto;
import com.fp.admintemplate.business.entity.PaymentManagementRequest;
import com.fp.admintemplate.business.service.PaymentManagementService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/paymentManagement")
public class PaymentManagementController {

    private final PaymentManagementService paymentManagementService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody PaymentManagementRequest request) {
        paymentManagementService.addPaymentManagement(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        paymentManagementService.deletePaymentManagement(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updatePaymentManagement(@RequestBody PaymentManagementRequest request) {
        paymentManagementService.updatePaymentManagement(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<PaymentManagementDto>> page(@RequestBody PaymentManagementRequest request) {
        Page<PaymentManagementDto> paymentManagementPage = paymentManagementService.getPaymentManagementPage(request);
        return CommonResult.data(paymentManagementPage);
    }


    @GetMapping("/one/{id}")
    public CommonResult<PaymentManagementDto> saveMenu(@PathVariable Long id) {
        PaymentManagementDto paymentManagementOne = paymentManagementService.getPaymentManagementOne(id);
        return CommonResult.data(paymentManagementOne);
    }
}
