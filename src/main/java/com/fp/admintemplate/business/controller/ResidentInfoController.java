package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.ResidentInfoDto;
import com.fp.admintemplate.business.entity.ResidentInfoRequest;
import com.fp.admintemplate.business.service.ResidentInfoService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/residentInfo")
public class ResidentInfoController {

    private final ResidentInfoService residentInfoService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody ResidentInfoRequest request) {
        residentInfoService.addResidentInfo(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        residentInfoService.deleteResidentInfo(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updateResidentInfo(@RequestBody ResidentInfoRequest request) {
        residentInfoService.updateResidentInfo(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<ResidentInfoDto>> page(@RequestBody ResidentInfoRequest request) {
        Page<ResidentInfoDto> residentInfoPage = residentInfoService.getResidentInfoPage(request);
        return CommonResult.data(residentInfoPage);
    }


    @GetMapping("/one/{id}")
    public CommonResult<ResidentInfoDto> saveMenu(@PathVariable Long id) {
        ResidentInfoDto residentInfoOne = residentInfoService.getResidentInfoOne(id);
        return CommonResult.data(residentInfoOne);
    }
}
