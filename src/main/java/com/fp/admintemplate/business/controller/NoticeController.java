package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.NoticeDto;
import com.fp.admintemplate.business.entity.NoticeRequest;
import com.fp.admintemplate.business.service.NoticeService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notice")
public class NoticeController {

    private final NoticeService noticeService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody NoticeRequest request) {
        noticeService.addNotice(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        noticeService.deleteNotice(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updateNotice(@RequestBody NoticeRequest request) {
        noticeService.updateNotice(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<NoticeDto>> page(@RequestBody NoticeRequest request) {
        Page<NoticeDto> noticePage = noticeService.getNoticePage(request);
        return CommonResult.data(noticePage);
    }

    @PostMapping("/list")
    public CommonResult<List<NoticeDto>> list(@RequestBody NoticeRequest request) {
        List<NoticeDto> noticeList = noticeService.getNoticeList(request);
        return CommonResult.data(noticeList);
    }


    @GetMapping("/one/{id}")
    public CommonResult<NoticeDto> saveMenu(@PathVariable Long id) {
        NoticeDto noticeOne = noticeService.getNoticeOne(id);
        return CommonResult.data(noticeOne);
    }
}
