package com.fp.admintemplate.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.RepairManagementDto;
import com.fp.admintemplate.business.entity.RepairManagementRequest;
import com.fp.admintemplate.business.service.RepairManagementService;
import com.fp.admintemplate.system.entity.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/repairManagement")
public class RepairManagementController {

    private final RepairManagementService repairManagementService;

    @PostMapping("/save")
    public CommonResult<Void> save(@RequestBody RepairManagementRequest request) {
        repairManagementService.addRepairManagement(request);
        return CommonResult.ok();
    }

    @GetMapping("/delete/{id}")
    public CommonResult<Void> delete(@PathVariable Long id) {
        repairManagementService.deleteRepairManagement(id);
        return CommonResult.ok();
    }

    @PostMapping("/update")
    public CommonResult<Void> updateRepairManagement(@RequestBody RepairManagementRequest request) {
        repairManagementService.updateRepairManagement(request);
        return CommonResult.ok();
    }

    @PostMapping("/page")
    public CommonResult<Page<RepairManagementDto>> page(@RequestBody RepairManagementRequest request) {
        Page<RepairManagementDto> repairManagementPage = repairManagementService.getRepairManagementPage(request);
        return CommonResult.data(repairManagementPage);
    }


    @GetMapping("/one/{id}")
    public CommonResult<RepairManagementDto> saveMenu(@PathVariable Long id) {
        RepairManagementDto repairManagementOne = repairManagementService.getRepairManagementOne(id);
        return CommonResult.data(repairManagementOne);
    }
}
