package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.NoticeDto;
import com.fp.admintemplate.business.entity.NoticeRequest;
import com.fp.admintemplate.business.entity.domain.Notice;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface NoticeMapper extends BaseMapper<Notice> {

    Page<NoticeDto> selectNoticePage(@Param("request") NoticeRequest request, @Param("page") Page<Object> page);

    List<NoticeDto> selectNoticeList(@Param("request") NoticeRequest request);

}




