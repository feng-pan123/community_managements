package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.PaymentManagementDto;
import com.fp.admintemplate.business.entity.PaymentManagementRequest;
import com.fp.admintemplate.business.entity.domain.PaymentManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface PaymentManagementMapper extends BaseMapper<PaymentManagement> {

    Page<PaymentManagementDto> selectPaymentManagementPage(@Param("request") PaymentManagementRequest request, @Param("page") Page<Object> page);

    List<PaymentManagementDto> selectPaymentManagementList(@Param("request") PaymentManagementRequest request);

}




