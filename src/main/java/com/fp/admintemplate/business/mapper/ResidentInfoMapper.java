package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.ResidentInfoDto;
import com.fp.admintemplate.business.entity.ResidentInfoRequest;
import com.fp.admintemplate.business.entity.domain.ResidentInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ResidentInfoMapper extends BaseMapper<ResidentInfo> {

    Page<ResidentInfoDto> selectResidentInfoPage(@Param("request") ResidentInfoRequest request, @Param("page") Page<Object> page);

    List<ResidentInfoDto> selectResidentInfoList(@Param("request") ResidentInfoRequest request);

}




