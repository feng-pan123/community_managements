package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.RepairManagementDto;
import com.fp.admintemplate.business.entity.RepairManagementRequest;
import com.fp.admintemplate.business.entity.domain.RepairManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface RepairManagementMapper extends BaseMapper<RepairManagement> {

    Page<RepairManagementDto> selectRepairManagementPage(@Param("request") RepairManagementRequest request, @Param("page") Page<Object> page);

    List<RepairManagementDto> selectRepairManagementList(@Param("request") RepairManagementRequest request);

}




