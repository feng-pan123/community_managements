package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.TeamMembersDto;
import com.fp.admintemplate.business.entity.TeamMembersRequest;
import com.fp.admintemplate.business.entity.domain.TeamMembers;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface TeamMembersMapper extends BaseMapper<TeamMembers> {

    Page<TeamMembersDto> selectTeamMembersPage(@Param("request") TeamMembersRequest request, @Param("page") Page<Object> page);

    List<TeamMembersDto> selectTeamMembersList(@Param("request") TeamMembersRequest request);

}




