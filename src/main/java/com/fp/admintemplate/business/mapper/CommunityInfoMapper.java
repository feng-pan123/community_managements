package com.fp.admintemplate.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fp.admintemplate.business.entity.CommunityInfoDto;
import com.fp.admintemplate.business.entity.CommunityInfoRequest;
import com.fp.admintemplate.business.entity.domain.CommunityInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface CommunityInfoMapper extends BaseMapper<CommunityInfo> {

    Page<CommunityInfoDto> selectCommunityInfoPage(@Param("request") CommunityInfoRequest request, @Param("page") Page<Object> page);

    List<CommunityInfoDto> selectCommunityInfoList(@Param("request") CommunityInfoRequest request);

}




