package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName(value ="payment_management")
@Data
public class PaymentManagement  {

    @TableId
    private Long id;

    /**
    * 住户ID
    */
    private Long residentId;

    /**
    * 缴费金额
    */
    private Double paymentAmount;

    /**
    * 缴费日期
    */
    private Date paymentDate;

    /**
    * 缴费状态（1: 未缴费，2: 已缴费，3: 欠费）
    */
    private String status;

    /**
    * 创建时间
    */
    private Date createTime;


}
