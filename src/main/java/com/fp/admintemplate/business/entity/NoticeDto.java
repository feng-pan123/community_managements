package com.fp.admintemplate.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class NoticeDto {

    private String id;

    /**
    * 标题
    */
    private String title;


    /**
    * 内容
    */
    private String content;

    private List<String> files;


    /**
    * 发布日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date publishDate;


    /**
    * 状态（1: 未发布，2: 已发布，3: 已撤回）
    */
    private String status;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
