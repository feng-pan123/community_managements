package com.fp.admintemplate.business.entity;

import com.fp.admintemplate.system.entity.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.fasterxml.jackson.annotation.JsonFormat;


import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class CommunityInfoRequest extends BaseRequest {

    private Long id;

    /**
     * 小区名称
     */
    private String communityName;


    /**
     * 地址
     */
    private String address;


    /**
     * 总楼栋数
     */
    private Integer totalBuildings;


    /**
     * 总单元数
     */
    private Integer totalUnits;


    /**
     * 总居民数
     */
    private Integer totalResidents;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


}
