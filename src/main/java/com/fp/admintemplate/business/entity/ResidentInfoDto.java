package com.fp.admintemplate.business.entity;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


@Data
public class ResidentInfoDto {

    private String id;

    /**
    * 住户姓名
    */
    private String name;


    /**
    * 性别（1: 男，2: 女）
    */
    private String gender;


    /**
    * 年龄
    */
    private Integer age;


    /**
    * 联系电话
    */
    private String phoneNumber;


    /**
    * 住址
    */
    private String address;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
