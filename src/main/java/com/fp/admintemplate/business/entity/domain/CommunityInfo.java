package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName(value ="community_info")
@Data
public class CommunityInfo  {

    @TableId
    private Long id;

    /**
    * 小区名称
    */
    private String communityName;

    /**
    * 地址
    */
    private String address;

    /**
    * 总楼栋数
    */
    private Integer totalBuildings;

    /**
    * 总单元数
    */
    private Integer totalUnits;

    /**
    * 总居民数
    */
    private Integer totalResidents;

    /**
    * 创建时间
    */
    private Date createTime;


}
