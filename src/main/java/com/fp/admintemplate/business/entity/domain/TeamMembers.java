package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName(value ="team_members")
@Data
public class TeamMembers  {

    @TableId
    private Long id;

    /**
    * USER_ID
    */
    private Long userId;

    /**
    * 成员姓名
    */
    private String name;

    /**
    * 性别
    */
    private String gender;

    /**
    * 邮箱
    */
    private String email;

    /**
    * 电话
    */
    private String phone;

    /**
    * 出生日期
    */
    private Date birthday;

    /**
    * 组织/公司名称
    */
    private String organization;

    /**
    * 主页头像
    */
    private String avatar;

    /**
    * 创建时间
    */
    private Date createTime;


}
