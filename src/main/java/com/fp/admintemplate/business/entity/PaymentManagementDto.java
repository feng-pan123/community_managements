package com.fp.admintemplate.business.entity;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


@Data
public class PaymentManagementDto {

    private String id;

    /**
    * 住户ID
    */
    private Long residentId;


    /**
    * 缴费金额
    */
    private Double paymentAmount;


    /**
    * 缴费日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date paymentDate;


    /**
    * 缴费状态（1: 未缴费，2: 已缴费，3: 欠费）
    */
    private String status;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
