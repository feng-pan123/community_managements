package com.fp.admintemplate.business.entity;

import com.fp.admintemplate.system.entity.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.fasterxml.jackson.annotation.JsonFormat;


import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class ResidentInfoRequest extends BaseRequest {

    private Long id;

    /**
    * 住户姓名
    */
    private String name;




    /**
    * 性别（1: 男，2: 女）
    */
    private String gender;




    /**
    * 年龄
    */
    private Integer age;




    /**
    * 联系电话
    */
    private String phoneNumber;




    /**
    * 住址
    */
    private String address;




    /**
    * 创建时间
    */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;





}
