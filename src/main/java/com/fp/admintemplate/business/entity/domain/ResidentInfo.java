package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName(value ="resident_info")
@Data
public class ResidentInfo  {

    @TableId
    private Long id;

    /**
    * 住户姓名
    */
    private String name;

    /**
    * 性别（1: 男，2: 女）
    */
    private String gender;

    /**
    * 年龄
    */
    private Integer age;

    /**
    * 联系电话
    */
    private String phoneNumber;

    /**
    * 住址
    */
    private String address;

    /**
    * 创建时间
    */
    private Date createTime;


}
