package com.fp.admintemplate.business.entity;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


@Data
public class CommunityInfoDto {

    private String id;

    /**
    * 小区名称
    */
    private String communityName;


    /**
    * 地址
    */
    private String address;


    /**
    * 总楼栋数
    */
    private Integer totalBuildings;


    /**
    * 总单元数
    */
    private Integer totalUnits;


    /**
    * 总居民数
    */
    private Integer totalResidents;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
