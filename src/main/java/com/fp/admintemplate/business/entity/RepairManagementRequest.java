package com.fp.admintemplate.business.entity;

import com.fp.admintemplate.system.entity.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.fasterxml.jackson.annotation.JsonFormat;


import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class RepairManagementRequest extends BaseRequest {

    private Long id;

    /**
    * 报修人ID
    */
    private Long residentId;




    /**
    * 报修类型
    */
    private String repairType;




    /**
    * 报修描述
    */
    private String repairDescription;




    /**
    * 报修日期
    */
        @JsonFormat(pattern = "yyyy-MM-dd")
    private Date repairDate;




    /**
    * 报修状态（1: 待处理，2: 处理中，3: 已完成）
    */
    private String status;




    /**
    * 创建时间
    */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;





}
