package com.fp.admintemplate.business.entity;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


@Data
public class TeamMembersDto {

    private String id;

    /**
    * USER_ID
    */
    private String userId;


    /**
    * 成员姓名
    */
    private String name;


    /**
    * 性别
    */
    private String gender;


    /**
    * 邮箱
    */
    private String email;


    /**
    * 电话
    */
    private String phone;


    /**
    * 出生日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;


    /**
    * 组织/公司名称
    */
    private String organization;


    /**
    * 主页头像
    */
    private String avatar;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
