package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName(value ="notice")
@Data
public class Notice  {

    @TableId
    private Long id;

    /**
    * 标题
    */
    private String title;

    /**
    * 内容
    */
    private String content;

    /**
    * 发布日期
    */
    private Date publishDate;

    /**
    * 状态（1: 未发布，2: 已发布，3: 已撤回）
    */
    private String status;

    /**
    * 创建时间
    */
    private Date createTime;


}
