package com.fp.admintemplate.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fp.admintemplate.system.entity.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class NoticeRequest extends BaseRequest {

    private Long id;

    /**
    * 标题
    */
    private String title;


    private List<String> files;

    /**
    * 内容
    */
    private String content;




    /**
    * 发布日期
    */
        @JsonFormat(pattern = "yyyy-MM-dd")
    private Date publishDate;




    /**
    * 状态（1: 未发布，2: 已发布，3: 已撤回）
    */
    private String status;




    /**
    * 创建时间
    */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;





}
