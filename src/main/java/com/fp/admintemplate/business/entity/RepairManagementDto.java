package com.fp.admintemplate.business.entity;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


@Data
public class RepairManagementDto {

    private String id;

    /**
    * 报修人ID
    */
    private String residentId;


    /**
    * 报修类型
    */
    private String repairType;


    /**
    * 报修描述
    */
    private String repairDescription;


    /**
    * 报修日期
    */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date repairDate;


    /**
    * 报修状态（1: 待处理，2: 处理中，3: 已完成）
    */
    private String status;


    /**
    * 创建时间
    */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
