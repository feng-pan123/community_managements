package com.fp.admintemplate.business.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName(value ="repair_management")
@Data
public class RepairManagement  {

    @TableId
    private Long id;

    /**
    * 报修人ID
    */
    private Long residentId;

    /**
    * 报修类型
    */
    private String repairType;

    /**
    * 报修描述
    */
    private String repairDescription;

    /**
    * 报修日期
    */
    private Date repairDate;

    /**
    * 报修状态（1: 待处理，2: 处理中，3: 已完成）
    */
    private String status;

    /**
    * 创建时间
    */
    private Date createTime;


}
