package com.fp.admintemplate.common.annotion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface FileInfoAction {

    String[] value() default "";

    String typeName() default "";


    ActionType action() default ActionType.SET;

}
