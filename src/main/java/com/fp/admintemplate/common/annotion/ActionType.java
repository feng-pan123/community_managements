package com.fp.admintemplate.common.annotion;

import lombok.Getter;

@Getter
public enum ActionType {

    SET,
    SAVE,
    DELETE,
    UPDATE;

    ActionType() {
    }
}
