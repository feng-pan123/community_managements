package com.fp.admintemplate.common.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.common.entity.PostCommentDto;
import com.fp.admintemplate.common.entity.PostRequest;
import com.fp.admintemplate.common.entity.domain.PostComment;

public interface PostCommentService extends IService<PostComment> {


    Page<PostCommentDto> getPostCommentPage(PostRequest request);

    void addPostComment(PostRequest request);

    void deletePostComment(PostRequest request);

    void updatePostComment(PostRequest request);

    PostCommentDto getPostCommentOne(PostRequest request);


}
