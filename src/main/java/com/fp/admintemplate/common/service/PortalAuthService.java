package com.fp.admintemplate.common.service;


import com.fp.admintemplate.common.entity.PortalUserRequest;

public interface PortalAuthService {


    String doLogin(PortalUserRequest graduateRequest);

    void doRegister(PortalUserRequest request);

}
