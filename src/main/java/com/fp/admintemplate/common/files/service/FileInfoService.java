package com.fp.admintemplate.common.files.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fp.admintemplate.common.files.entity.FilesInfo;

import java.util.List;
import java.util.Map;


public interface FileInfoService extends IService<FilesInfo> {

    List<String> getOwnerFileBase64(Long ownerId, String type);

    void editOneFIleBase64(Long ownerId, String type, String fileBase64);

    void deleteFileInfos(Long ownerId, String type);


    void saveFilesInfoBase64(Long ownerId, String type, List<String> request);

    void saveOneFileBase64(Long ownerId, String type, String fileBase64);

    Map<Long, List<String>> getOwnerListFiles(List<Long> ownerId, String type);

    <T> void setFileListBase64(List<T> ownerPageList, String type);

    <T> void setOneFilesBase64(T t, String type);


}
