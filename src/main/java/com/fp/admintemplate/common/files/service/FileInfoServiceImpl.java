package com.fp.admintemplate.common.files.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fp.admintemplate.common.files.entity.FilesInfo;
import com.fp.admintemplate.common.files.mapper.FilesInfoMapper;
import com.fp.admintemplate.system.exception.ServiceException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class FileInfoServiceImpl extends ServiceImpl<FilesInfoMapper, FilesInfo>
        implements FileInfoService {


    @Override
    public void saveOneFileBase64(Long ownerId, String type, String fileBase64) {
        if (StrUtil.isEmpty(fileBase64)) return;
        FilesInfo filesInfo = new FilesInfo();
        filesInfo.setOwnerId(ownerId);
        filesInfo.setBase64Content(fileBase64);
        filesInfo.setType(type);
        filesInfo.setCreateTime(new Date());
        this.baseMapper.insert(filesInfo);
    }

    @Override
    public void saveFilesInfoBase64(Long ownerId, String type, List<String> request) {
        if (CollUtil.isEmpty(request)) return;
        List<FilesInfo> infoList = request.stream().map(x -> {
            FilesInfo filesInfo = new FilesInfo();
            filesInfo.setOwnerId(ownerId);
            filesInfo.setBase64Content(x);
            filesInfo.setType(type);
            filesInfo.setCreateTime(new Date());
            return filesInfo;
        }).toList();
        infoList.forEach(this.baseMapper::insert);
    }

    @Override
    public void deleteFileInfos(Long ownerId, String type) {
        LambdaQueryWrapper<FilesInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilesInfo::getOwnerId, ownerId);
        queryWrapper.eq(FilesInfo::getType, type);
        this.baseMapper.delete(queryWrapper);
    }

    @Override
    public void editOneFIleBase64(Long ownerId, String type, String fileBase64) {
        if (StrUtil.isEmpty(fileBase64)) {
            this.deleteFileInfos(ownerId, type);
            return;
        }
        LambdaQueryWrapper<FilesInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilesInfo::getOwnerId, ownerId);
        queryWrapper.eq(FilesInfo::getType, type);
        this.baseMapper.delete(queryWrapper);
        this.saveOneFileBase64(ownerId,type,fileBase64);
    }



    @Override
    public List<String> getOwnerFileBase64(Long ownerId, String type) {
        LambdaQueryWrapper<FilesInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilesInfo::getType, type);
        queryWrapper.in(FilesInfo::getOwnerId, ownerId);

        List<FilesInfo> filesInfos = this.baseMapper.selectList(queryWrapper);
        if (CollUtil.isEmpty(filesInfos)) throw new ServiceException("未上传文件");

        return filesInfos.stream().map(FilesInfo::getBase64Content).toList();
    }

    @Override
    public <T> void setOneFilesBase64(T t, String type) {
        Long id =  Long.parseLong((String)ReflectUtil.getFieldValue(t, "id"));

        LambdaQueryWrapper<FilesInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilesInfo::getOwnerId,id);
        queryWrapper.eq(FilesInfo::getType,type);
        List<FilesInfo> filesInfos = this.baseMapper.selectList(queryWrapper);

        if (CollUtil.isEmpty(filesInfos)) {
            ReflectUtil.setFieldValue(t, "files", new ArrayList<String>());
        }
        List<String> list = filesInfos.stream().map(FilesInfo::getBase64Content).toList();
        ReflectUtil.setFieldValue(t, "files", list);
    }

    @Override
    public <T> void setFileListBase64(List<T> ownerPageList, String type) {
        List<Long> ownerIds = ownerPageList.stream().map(x -> Long.parseLong((String) ReflectUtil.getFieldValue(x, "id"))).toList();
        Map<Long, List<String>> ownerListImagesUrl = this.getOwnerListFiles(ownerIds, type);

        ownerPageList.forEach(x -> {
            Long ownerId = Long.parseLong((String) ReflectUtil.getFieldValue(x, "id"));
            List<String> fileBase64 = ownerListImagesUrl.get(ownerId);
            ReflectUtil.setFieldValue(x, "files", fileBase64);
        });
    }
    @Override
    public Map<Long, List<String>> getOwnerListFiles(List<Long> ownerId, String type) {
        LambdaQueryWrapper<FilesInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilesInfo::getType, type);
        queryWrapper.in(FilesInfo::getOwnerId, ownerId);

        List<FilesInfo> imagesInfos = this.baseMapper.selectList(queryWrapper);
        return imagesInfos.stream().collect(Collectors.groupingBy(FilesInfo::getOwnerId,
                Collectors.mapping(FilesInfo::getBase64Content, Collectors.toList())));
    }

}




